FROM python:3.8-slim


RUN mkdir /code
WORKDIR /core
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .
