from django.urls import path, include


urlpatterns = [
    path('', include('zoomwatcher.urls', namespace="zoomwatcher")),
]
