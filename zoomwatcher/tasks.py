import logging

from django.conf import settings
from config.celery import app
from zoomwatcher.models import LiveMeeting
from zoomwatcher.zoom import Client, Role

log = logging.getLogger(__name__)


@app.task
def fetch_meetings():
    log.info(f"Fetching meetings")

    client = Client(api_token=settings.ZOOM_API)
    members = client.get_members_by_role(Role.Owner)
    members.extend(client.get_members_by_role(Role.Admin))
    members.extend(client.get_members_by_role(Role.Member))

    log.info(f"Total number of members: {len(members)}")

    for member in members:
        for meeting in client.get_user_meetings(member["id"]):
            meeting_status = client.get_meeting_state(meeting["id"])

            if meeting_status == "started":
                total_participants = client.get_total_participants(meeting["id"])
                log.info(f"Meetig ID: {meeting['id']}; Total participants: {total_participants}; Status: {meeting_status}")
                LiveMeeting.objects.update_or_create(
                    meeting_id=meeting["id"],
                    defaults=dict(participants=total_participants))
            else:
                log.debug(f"Meetig ID: {meeting['id']}; Total participants: {0}; Status: {meeting_status}")
                LiveMeeting.objects.filter(meeting_id=meeting["id"]).delete()


# Schedule periodic task
# TODO: make 70 an env ver
app.add_periodic_task(70, fetch_meetings.s(), name="Fetch meetings task")

# Trigger it first time to save time on waiting for the scheduled task to run
fetch_meetings.s().apply_async(countdown=1)
