import logging

from django.conf import settings
from django.shortcuts import redirect
from django.views.generic import ListView

from zoomwatcher.models import LiveMeeting, MeetingDetails
from zoomwatcher.zoom import Client

log = logging.getLogger(__name__)


class HomePage(ListView):
    template_name = "zoomwatcher/home.html"
    model = LiveMeeting
    paginate_by = 25

    def get_queryset(self):
        return (super().get_queryset()
                .order_by("-participants")
                .prefetch_related("details"))

    def post(self, request, *args, **kwargs):
        meeting_id = request.POST["meeting-id"]
        log.debug(f"Request to end meeting '{meeting_id}'")
        count = MeetingDetails.objects.filter(meeting__meeting_id=meeting_id).update(is_asked_to_end=True)

        if count:
            Client(settings.ZOOM_API).end_meeting(meeting_id)

        return redirect('zoomwatcher:home')
