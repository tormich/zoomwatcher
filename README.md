# Jennifer's online clinic Zoom watcher

A quick POC of an administrative dashboard that keeps track of Jennifer's therapist's live sessions.

This project is meant for small office purposes. 

While not relying on the webhook makes it perfect to set up on any machine, 
the drawbacks are that it takes time to update the state.
This issue can partially be fixed by spreading meeting IDs to multiple workers.
In our particular case, however,  this shouldn't be a problem.


## Included in the box

- Dockerfile that builds the image containing both the code for the webserver and for the celery worker.
- docker-compose.yml that you are expected to run.  
- Project code
- Example of the`.env` (dot env) file named `.env_example`.


## Run
 
To run this project you will need to:

1. Install the docker and docker-compose.
1. Set the environment:
    1. Rename `.env_example` file to `.env`
    1. Open it for editing 
    1. Replace `<ZOOM API TOKEN HERE>` with your zoom api token 
1. Run `docker-compose up`

__It will take a couple of seconds (or minutes) to fully start.__

When services are up you can access the dashboard at http://localhost:8000

![Dashboard Screenshot](dashboard_screenshot.png)


## What happens next?

`docker-compose up` command will bring up 4 services:

- Web server (serves the dashboard)
- Celery workers (update meetings every 70(*) seconds)
- Redis (broker for celery)
- Postgres (💞 The Database)

The `fetch_meetings` task will start immediately.

It might take a minute or two before you can see the updated no. of participants on the dashboard.


## How does it work

The `fetch_meetings` will first get all the members using the `/roles/{role_id}/members` api.

Then:
- for each member it will get meetings from the `/users/{user_id}/meetings` api
- get meeting state from `/meetings/{meeting_id}`
- for each meeting
  - if meeting status `==` "started" - get total amount of participants from `/metrics/meetings/{meeting_id}/participants/qos` and update the database
  - if meeting status `!=` "started" - delete from database
   