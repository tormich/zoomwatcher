from django.urls import path

from zoomwatcher.views import HomePage

app_name = 'zoomwatcher'
urlpatterns = [
    path('', HomePage.as_view(), name='home')
]
