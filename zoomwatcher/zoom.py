import logging
from enum import Enum
from typing import Literal, Dict, List

import attr
import requests

log = logging.getLogger(__name__)


class APIError(Exception):
    pass


class HTTPError(APIError):
    pass


class Role(Enum):
    Owner = 0
    Admin = 1
    Member = 2


@attr.s(auto_attribs=True)
class Client:
    api_token: str = attr.ib(repr=False)
    name = "zoom_api_client"
    base_url: str = "https://api.zoom.us/v2"
    timezone: str = attr.ib(default="UTC")

    def get_members_by_role(self, role: Role) -> List[Dict]:
        role_id = role.value
        endpoint = f"/roles/{role_id}/members"
        members = self.get_all_pages(endpoint, "members")
        return members

    def get_user_meetings(self, user_id) -> List[Dict]:
        endpoint = f"/users/{user_id}/meetings"
        meetings = self.get_all_pages(endpoint, "meetings")
        return meetings

    def get_meeting_state(self, meeting_id) -> Literal["started", "waiting", "not_found"]:
        endpoint = f"/meetings/{meeting_id}"
        return self.get(endpoint, "GET").json().get("status", "not_found")

    def get_total_participants(self, meeting_id) -> int:
        endpoint = f"/metrics/meetings/{meeting_id}/participants/qos"
        return self.get(endpoint, "GET").json().get("total_records", 0)

    def end_meeting(self, meeting_id: str) -> None:
        endpoint = f"/meetings/{meeting_id}/status"
        log.debug(endpoint)
        action = {"action": "end"}
        self.get(endpoint, "PUT", body=action, expect_json=False)

    def get_all_pages(self, endpoint: str, field_name: str):
        next_page_token = ""
        result = []
        while True:
            query_params = {"next_page_token": next_page_token}
            res = self.get(endpoint, "GET", query_params).json()
            result.extend(res.get(field_name))
            next_page_token = res.get("next_page_token")
            if not next_page_token:
                break
        return result

    def get(
            self,
            endpoint: str,
            method: Literal["GET", "POST", "PATCH", "DELETE", "PUT"],
            query: Dict = None,
            body: Dict = None,
            raise_on_error=True,
            expect_json=True
    ) -> requests.Response:
        allowed_methods = "GET POST PATCH DELETE PUT".split()
        if method not in allowed_methods:
            raise ValueError(
                f'Invalid method: {method}. Must be one of {", ".join(allowed_methods)}'
            )
        headers = {"Authorization": f"Bearer {self.api_token}"}
        url = self.base_url + endpoint
        log.debug(f"Making {method} request to {endpoint}")
        session = requests.Session()

        r = session.request(method, url, headers=headers, params=query, json=body)

        if 200 <= r.status_code < 300:
            return r

        if expect_json:
            try:
                message = r.json()["_error"]["message"]
            except KeyError:
                message = r.json()
        else:
            message = r.text

        log.debug(f"Unsuccessful request to {r.url}: [{r.status_code}] {message}")

        log.debug(f"Full response: {r.json() if expect_json else r.text}")
        log.debug(f"Headers: {r.headers}")
        log.debug(f"Params: {query}")
        log.debug(f"Body: {body}")
        if not raise_on_error:
            log.warning(
                f"raise_on_error is False, ignoring API error and returning response"
            )
            return r

        if r.status_code in [400, 401, 404, 405, 409]:
            raise HTTPError(message)
        else:
            raise APIError(message)
