from django.apps import AppConfig


class ZoomwatcherConfig(AppConfig):
    name = 'zoomwatcher'
