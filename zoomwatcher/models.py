import logging

from django.db import models


log = logging.getLogger(__name__)


class LiveMeeting(models.Model):
    """ This model represents a meeting that has started
        The meeting ID can be a 10 or 11-digit number.
        https://support.zoom.us/hc/en-us/articles/201362373-Meeting-ID
    """
    meeting_id = models.TextField(max_length=12, unique=True)
    participants = models.IntegerField()

    def get_details(self):
        if not self.has_details():
            return
        return self.details.get()

    def has_details(self):
        return self.details.exists()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

        """ If somebody joins a meeting after we asked to end it 
            we might get into a state where the meeting is alive and has participants 
            but we show it as if we are still waiting for it to end.
            
            Reset "is_asked_to_end" flag to false.
            It might be that we are still working on the state snapshot we got from zoom before 
            asking to end a meeting. In this case resetting this flag will make us present this
            meeting as if it is alive and has participants when in fact it has ended.
            But it will disappear as soon as we get the next update from zoom.
             
            Worst case scenario - user can press another time on the "stop" button
        """
        MeetingDetails.objects.update_or_create(meeting=self, defaults=dict(is_asked_to_end=False, meeting=self))


class MeetingDetails(models.Model):
    """ Here we store additional information about zoom meetings.
    """
    meeting = models.ForeignKey(to=LiveMeeting, on_delete=models.CASCADE, related_name="details")
    is_asked_to_end = models.BooleanField(default=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"MeetingID: {self.meeting.meeting_id}; is_asked_to_end: {self.is_asked_to_end}"
