from django.test import TestCase

from zoomwatcher.models import LiveMeeting


class MeetingMetaTest(TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_something_that_will_pass(self):
        lm = LiveMeeting.objects.update_or_create(
            meeting_id="TEST_TEST_123",
            defaults={"meeting_id": "TEST_TEST_123", "participants": 2})
        self.assertEqual(False, lm[0].meetingmeta.get().is_asked_to_end)
