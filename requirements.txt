django-celery==3.1.17
celery==5.0.5
Django==3.1.7
requests==2.25.1
redis==3.4.1
psycopg2-binary
attrs~=20.3.0